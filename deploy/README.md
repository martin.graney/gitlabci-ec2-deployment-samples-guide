**Notes**:

This directory contains:

- `extras` => files necessary for the deployment
	- `aws_code_deploy`
		- `appspec.yml` => specification file for CodeDeploy - to be included at the root of the distribution zip
	- `my_app.sh` => application run helper file, used by hook sample scripts provided
- `scripts` => CodeDeploy hook scripts referenced by the sample `appspec.yml` - this directory must be placed at the root of the distribution zip
	- `after_install.sh` => hook script
	- `app_start.sh` => hook script
	- `app_stop.sh` => hook script
	- `before_install.sh` => hook script
	- `validate_service.sh` => hook script